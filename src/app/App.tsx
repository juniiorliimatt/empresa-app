import {Container} from 'react-bootstrap';
import {BrowserRouter} from 'react-router-dom';

import {MyRoutes} from './routes';
import MyNav from './shared/components/mynav';

export const App = () => {
  return (
    <BrowserRouter>
      <MyNav />
      <Container>
        <MyRoutes />
      </Container>
    </BrowserRouter>
  );
};
