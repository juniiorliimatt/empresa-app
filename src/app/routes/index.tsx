import { Route, Routes } from 'react-router-dom';
import DetailEmployee from '../pages/employee/detail-employeecopy';
import EditEmployee from '../pages/employee/edit-employee';

import EmployeeComponent from '../pages/employee/list-employee';
import NewEmployee from '../pages/employee/new-employee';
import DetailOffice from '../pages/office/detail-office';
import EditOffice from '../pages/office/edit-office';
import OfficeComponent from '../pages/office/list-office';
import NewOffice from '../pages/office/new-office';
import DetailSector from '../pages/sector/detail-sector';
import EditSector from '../pages/sector/edit-sector';
import SectorComponent from '../pages/sector/list-sector';
import NewSector from '../pages/sector/new-sector';

export const MyRoutes = () => {
  return (
    <Routes>
      <Route path="/employees" element={<EmployeeComponent />} />
      <Route path="/offices" element={<OfficeComponent />} />
      <Route path="/sectors" element={<SectorComponent />} />

      <Route path="/new_sector" element={<NewSector />} />
      <Route path="/detail_sector/:id" element={<DetailSector />} />
      <Route path="/edit_sector/:id" element={<EditSector />} />

      <Route path="/new_office" element={<NewOffice />} />
      <Route path="/detail_office/:id" element={<DetailOffice />} />
      <Route path="/edit_office/:id" element={<EditOffice />} />

      <Route path="/new_employee" element={<NewEmployee />} />
      <Route path="/detail_employee/:id" element={<DetailEmployee />} />
      <Route path="/edit_employee/:id" element={<EditEmployee />} />
    </Routes>
  );
};
