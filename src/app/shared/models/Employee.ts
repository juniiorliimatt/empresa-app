import { Office } from './Office';
import { Sector } from './Sector';

export interface Employee {
  id: number;
  name: string;
  cpf: string;
  email: string;
  sex: string;
  birthDate: Date;
  hiringDate: Date;
  situation: boolean;
  sector: Sector;
  office: Office;
}
