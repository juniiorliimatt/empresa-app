export interface Office {
  id: number;
  name: string;
  description: string;
}
