import { ChangeEvent, useEffect, useState } from 'react';
import { Button, Form } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import { Employee } from '../../models/Employee';
import { Office } from '../../models/Office';
import { Sector } from '../../models/Sector';
import api from '../../services/api';

interface Props {
  id: string | undefined;
}

const FormEmployee = (prop: Props) => {
  const navigate = useNavigate();
  const { id } = prop;
  const [employee, setEmployee] = useState<Employee>({} as Employee);
  const [sectors, setSectors] = useState<Sector[]>([]);
  const [officers, setOfficers] = useState<Office[]>([]);

  useEffect(() => {
    if (id != undefined) {
      findEmployee();
    }
    loadOffices();
    loadSectors();
  }, []);

  function updateEmployee(e: ChangeEvent<HTMLInputElement>) {
    setEmployee({
      ...employee,
      [e.target.name]: e.target.value,
    });
  }

  async function findEmployee() {
    const response = await api.get<Employee>(`/employee/${id}`);
    setEmployee({
      id: response.data.id,
      name: response.data.name,
      cpf: response.data.cpf,
      email: response.data.email,
      sex: response.data.sex,
      birthDate: response.data.birthDate,
      hiringDate: response.data.hiringDate,
      situation: response.data.situation,
      sector: response.data.sector,
      office: response.data.office,
    });
  }

  async function loadSectors() {
    const response = await api.get('/sector/');
    setSectors(response.data);
  }

  async function loadOffices() {
    const response = await api.get('/office/');
    setOfficers(response.data);
  }

  async function onSubmit(e: ChangeEvent<HTMLFormElement>) {
    e.preventDefault();

    console.log(employee);

    if (id !== undefined) await api.put(`/employee/${id}`, employee);
    if (id === undefined) await api.post('/employee', employee);

    navigate('/employees');
  }

  return (
    <Form onSubmit={onSubmit}>
      <Form.Group className="mb-3" controlId="name">
        <Form.Label>Name</Form.Label>
        <Form.Control
          value={employee.name || ''}
          name="name"
          type="text"
          placeholder="Name of employee"
          onChange={(e: ChangeEvent<HTMLInputElement>) => updateEmployee(e)}
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="cpf">
        <Form.Label>CPF</Form.Label>
        <Form.Control
          value={employee.cpf || ''}
          name="cpf"
          type="text"
          placeholder="CPF"
          onChange={(e: ChangeEvent<HTMLInputElement>) => updateEmployee(e)}
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="email">
        <Form.Label>E-mail</Form.Label>
        <Form.Control
          name="email"
          type="email"
          placeholder="E-Mail"
          onChange={(e: ChangeEvent<HTMLInputElement>) => updateEmployee(e)}
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="sex">
        <Form.Label>Sex</Form.Label>
        <Form.Control
          value={employee.sex || ''}
          name="sex"
          type="text"
          placeholder="Sex"
          onChange={(e: ChangeEvent<HTMLInputElement>) => updateEmployee(e)}
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="birthdate">
        <Form.Label>Birth Date</Form.Label>
        <Form.Control
          name="birthdate"
          type="date"
          placeholder="Bithdate"
          onChange={(e: ChangeEvent<HTMLInputElement>) => updateEmployee(e)}
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="hiringdate">
        <Form.Label>Hiring Date</Form.Label>
        <Form.Control
          name="hiringdate"
          type="date"
          placeholder="Hiring Date"
          onChange={(e: ChangeEvent<HTMLInputElement>) => updateEmployee(e)}
        />
      </Form.Group>

      <Form.Group
        className="mb-3"
        controlId="hiringdate"
        onChange={(e: ChangeEvent<HTMLInputElement>) => updateEmployee(e)}
      >
        <Form.Label>Situation</Form.Label> <br />
        <Form.Check
          inline
          label="Active"
          value="true"
          type="radio"
          name="situation"
          id="inline-radio-male"
        />
        <Form.Check
          inline
          label="Inactive"
          value="false"
          type="radio"
          name="situation"
          id="inline-radio-female"
        />
      </Form.Group>

      <Form.Group
        className="mb-3"
        controlId="sector"
        onChange={(e: ChangeEvent<HTMLInputElement>) => updateEmployee(e)}
      >
        <Form.Label>Sector</Form.Label>
        <Form.Select name="sector">
          {sectors.map((sector) => (
            <option key={sector.id} value={sector.id}>
              {sector.name}
            </option>
          ))}
        </Form.Select>
      </Form.Group>

      <Form.Group
        className="mb-3"
        controlId="office"
        onChange={(e: ChangeEvent<HTMLInputElement>) => updateEmployee(e)}
      >
        <Form.Label>Office</Form.Label>
        <Form.Select name="office">
          {officers.map((office) => (
            <option key={office.id} value={office.id}>
              {office.name}
            </option>
          ))}
        </Form.Select>
      </Form.Group>

      <Button variant="dark" type="submit">
        Save
      </Button>
    </Form>
  );
};

export default FormEmployee;
