import { Container, Navbar, Nav } from 'react-bootstrap';
import { Link } from 'react-router-dom';

const MyNav = () => {
  return (
    <Navbar bg="dark" variant="dark">
      <Container>
        <Navbar.Brand>Company System</Navbar.Brand>
        <Nav className="me-auto">
          <Nav.Link as={Link} to="/employees">
            Employee
          </Nav.Link>
          <Nav.Link as={Link} to="/offices">
            Office
          </Nav.Link>
          <Nav.Link as={Link} to="/sectors">
            Sector
          </Nav.Link>
        </Nav>
      </Container>
    </Navbar>
  );
};

export default MyNav;
