import { ChangeEvent, useEffect, useState } from 'react';
import { Button, Form } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import { Sector } from '../../models/Sector';
import api from '../../services/api';

interface Props {
  id: string | undefined;
}

const FormSector = (prop: Props) => {
  const navigate = useNavigate();
  const { id } = prop;
  const [sector, setSector] = useState<Sector>({} as Sector);

  useEffect(() => {
    if (id != undefined) {
      findSector();
    }
  }, []);

  function updateSector(e: ChangeEvent<HTMLInputElement>) {
    setSector({
      ...sector,
      [e.target.name]: e.target.value,
    });
  }

  async function findSector() {
    const response = await api.get<Sector>(`/sector/${id}`);
    setSector({
      id: response.data.id,
      name: response.data.name,
      description: response.data.description,
    });
  }

  async function onSubmit(e: ChangeEvent<HTMLFormElement>) {
    e.preventDefault();

    if (id !== undefined) await api.put(`/sector/${id}`, sector);
    if (id === undefined) await api.post('/sector', sector);

    navigate('/sectores');
  }

  return (
    <Form onSubmit={onSubmit}>
      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label>Name</Form.Label>
        <Form.Control
          value={sector.name || ''}
          name="name"
          type="text"
          placeholder="Name of sector"
          onChange={(e: ChangeEvent<HTMLInputElement>) => updateSector(e)}
        />
        <Form.Text className="text-muted">
          A name for your industry, in short.
        </Form.Text>
      </Form.Group>

      <Form.Group className="mb-3" controlId="formBasicPassword">
        <Form.Label>Description</Form.Label>
        <Form.Control
          as="textarea"
          value={sector.description || ''}
          name="description"
          placeholder="Description of sector tarefa"
          onChange={(e: ChangeEvent<HTMLInputElement>) => updateSector(e)}
        />
        <Form.Text className="text-muted">
          Describe what the sector will look like.
        </Form.Text>
      </Form.Group>
      <Button variant="dark" type="submit">
        Save
      </Button>
    </Form>
  );
};

export default FormSector;
