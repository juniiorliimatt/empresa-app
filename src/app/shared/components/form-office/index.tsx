import { ChangeEvent, useEffect, useState } from 'react';
import { Button, Form } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import { Office } from '../../models/Office';
import api from '../../services/api';

interface Props {
  id: string | undefined;
}

const FormOffice = (prop: Props) => {
  const navigate = useNavigate();
  const { id } = prop;
  const [office, setOffice] = useState<Office>({} as Office);

  useEffect(() => {
    if (id != undefined) {
      findOffice();
    }
  }, []);

  function updateOffice(e: ChangeEvent<HTMLInputElement>) {
    setOffice({
      ...office,
      [e.target.name]: e.target.value,
    });
  }

  async function findOffice() {
    const response = await api.get<Office>(`/office/${id}`);
    setOffice({
      id: response.data.id,
      name: response.data.name,
      description: response.data.description,
    });
  }

  async function onSubmit(e: ChangeEvent<HTMLFormElement>) {
    e.preventDefault();

    if (id !== undefined) await api.put(`/office/${id}`, office);
    if (id === undefined) await api.post('/office', office);

    navigate('/offices');
  }

  return (
    <Form onSubmit={onSubmit}>
      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label>Name</Form.Label>
        <Form.Control
          value={office.name || ''}
          name="name"
          type="text"
          placeholder="Name of office"
          onChange={(e: ChangeEvent<HTMLInputElement>) => updateOffice(e)}
        />
        <Form.Text className="text-muted">
          A name for your industry, in short.
        </Form.Text>
      </Form.Group>

      <Form.Group className="mb-3" controlId="formBasicPassword">
        <Form.Label>Description</Form.Label>
        <Form.Control
          as="textarea"
          value={office.description || ''}
          name="description"
          placeholder="Description of office"
          onChange={(e: ChangeEvent<HTMLInputElement>) => updateOffice(e)}
        />
        <Form.Text className="text-muted">
          Describe what the office will look like.
        </Form.Text>
      </Form.Group>
      <Button variant="dark" type="submit">
        Save
      </Button>
    </Form>
  );
};

export default FormOffice;
