import { Button } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';

import './style.css';

interface Props {
  readonly link: string;
  readonly button: string;
  readonly title: string;
}

const Header: React.FC<Props> = ({ link, button, title }) => {
  const navigate = useNavigate();

  function handlerNavigate() {
    navigate(`${link}`);
  }

  return (
    <div className="sector-header">
      <div>
        <h2>{title}</h2>
      </div>
      <div>
        <h2>
          <Button variant="dark" size="sm" onClick={handlerNavigate}>
            {button}
          </Button>
        </h2>
      </div>
    </div>
  );
};

export default Header;
