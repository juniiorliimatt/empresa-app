import FormEmployee from '../../../shared/components/form-employee';
import Header from '../../../shared/components/header';

const NewEmployee = () => {
  return (
    <>
      <Header title="New Employee" button="back" link="/employees" />
      <FormEmployee id={undefined} />
    </>
  );
};

export default NewEmployee;
