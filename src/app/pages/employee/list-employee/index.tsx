import Header from '../../../shared/components/header';

const EmployeeComponent = () => {
  return (
    <div>
      <Header
        title="List of Eployees"
        button="new employee"
        link="/new_employee"
      />
    </div>
  );
};

export default EmployeeComponent;
