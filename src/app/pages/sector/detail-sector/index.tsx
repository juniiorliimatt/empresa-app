import { useEffect, useState } from 'react';
import { Card } from 'react-bootstrap';
import { useParams } from 'react-router-dom';

import Header from '../../../shared/components/header';
import { Sector } from '../../../shared/models/Sector';
import api from '../../../shared/services/api';

const DetailSector = () => {
  const [sector, setSector] = useState<Sector>({} as Sector);
  const { id } = useParams();

  useEffect(() => {
    loadSector();
  }, []);

  async function loadSector() {
    const response = await api.get(`/sector/${id}`);
    setSector(response.data);
  }

  return (
    <>
      <Header title="Detail Sector" button="back" link="/sectors" />
      <Card style={{ width: '18rem' }}>
        <Card.Body>
          <Card.Title>{sector.name}</Card.Title>
          <Card.Subtitle className="mb-2 text-muted">
            Id: {sector.id}
          </Card.Subtitle>
          <Card.Text>{sector.description}</Card.Text>
        </Card.Body>
      </Card>
    </>
  );
};

export default DetailSector;
