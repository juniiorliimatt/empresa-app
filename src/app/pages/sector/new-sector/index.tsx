import Header from '../../../shared/components/header';
import FormSector from '../../../shared/components/form-sector';

const NewSector = () => {
  return (
    <>
      <Header title="New Sector" button={'back'} link={'/sectors'} />
      <FormSector id={undefined} />
    </>
  );
};

export default NewSector;
