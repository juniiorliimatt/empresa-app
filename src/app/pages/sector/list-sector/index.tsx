import { useEffect, useState } from 'react';
import { Button, Table } from 'react-bootstrap';
import { FaInfo, FaRegEdit, FaTrashAlt } from 'react-icons/fa';
import { useNavigate } from 'react-router-dom';

import Header from '../../../shared/components/header';
import { Sector } from '../../../shared/models/Sector';
import api from '../../../shared/services/api';

const SectorComponent = () => {
  const navigate = useNavigate();
  const [sectors, setSectors] = useState<Sector[]>([]);

  useEffect(() => {
    loadSectors();
  }, []);

  function detailSector(id: number) {
    navigate(`/detail_sector/${id}`);
  }

  function editSector(id: number) {
    navigate(`/edit_sector/${id}`);
  }

  async function deleteSector(id: number) {
    await api.delete(`/sector/${id}`);
    loadSectors();
  }

  async function loadSectors() {
    const response = await api.get('/sector/');
    setSectors(response.data);
  }

  return (
    <div>
      <Header title="List Sector" button="new sector" link="/new_sector" />
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {sectors.map((sector) => (
            <tr key={sector.id}>
              <td>{sector.id}</td>
              <td>{sector.name}</td>
              <td className="text-center">
                <Button
                  variant="info"
                  size="sm"
                  onClick={() => {
                    detailSector(sector.id);
                  }}
                >
                  <FaInfo />
                </Button>{' '}
                <Button
                  variant="success"
                  size="sm"
                  onClick={() => {
                    editSector(sector.id);
                  }}
                >
                  <FaRegEdit />
                </Button>{' '}
                <Button
                  variant="danger"
                  size="sm"
                  onClick={() => {
                    deleteSector(sector.id);
                  }}
                >
                  <FaTrashAlt />
                </Button>{' '}
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </div>
  );
};

export default SectorComponent;
