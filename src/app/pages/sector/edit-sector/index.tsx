import { useParams } from 'react-router-dom';

import FormSector from '../../../shared/components/form-sector';
import Header from '../../../shared/components/header';

const EditSector = () => {
  const { id } = useParams();

  return (
    <>
      <Header title="Edit Sector" button="back" link="/sectors" />
      <FormSector id={id} />
    </>
  );
};

export default EditSector;
