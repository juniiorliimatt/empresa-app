import { useEffect, useState } from 'react';
import { Card } from 'react-bootstrap';
import { useParams } from 'react-router-dom';
import Header from '../../../shared/components/header';
import { Office } from '../../../shared/models/Office';
import api from '../../../shared/services/api';

const DetailOffice = () => {
  const [office, setOffice] = useState<Office>({} as Office);
  const { id } = useParams();

  useEffect(() => {
    loadOffice();
  }, []);

  async function loadOffice() {
    const response = await api.get(`/office/${id}`);
    setOffice(response.data);
  }

  return (
    <>
      <Header title="Detail Office" button="back" link="/offices" />
      <Card style={{ width: '18rem' }}>
        <Card.Body>
          <Card.Title>{office.name}</Card.Title>
          <Card.Subtitle className="mb-2 text-muted">
            Id: {office.id}
          </Card.Subtitle>
          <Card.Text>{office.description}</Card.Text>
        </Card.Body>
      </Card>
    </>
  );
};

export default DetailOffice;
