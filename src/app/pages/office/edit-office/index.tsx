import { useParams } from 'react-router-dom';
import FormOffice from '../../../shared/components/form-office';
import Header from '../../../shared/components/header';

const EditOffice = () => {
  const { id } = useParams();
  return (
    <>
      <Header title="Edit Office" button="back" link="/offices" />
      <FormOffice id={id} />
    </>
  );
};

export default EditOffice;
