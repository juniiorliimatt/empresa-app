import FormOffice from '../../../shared/components/form-office';
import Header from '../../../shared/components/header';

const NewOffice = () => {
  return (
    <>
      <Header title="New Office" button="back" link="/officers" />
      <FormOffice id={undefined} />
    </>
  );
};

export default NewOffice;
