import { useEffect, useState } from 'react';
import { Button, Table } from 'react-bootstrap';
import { FaInfo, FaRegEdit, FaTrashAlt } from 'react-icons/fa';
import { useNavigate } from 'react-router-dom';

import Header from '../../../shared/components/header';
import { Office } from '../../../shared/models/Office';
import api from '../../../shared/services/api';

const OfficeComponent = () => {
  const navigate = useNavigate();
  const [offices, setOffices] = useState<Office[]>([]);

  useEffect(() => {
    loadOffices();
  }, []);

  function detailOffice(id: number) {
    navigate(`/detail_office/${id}`);
  }

  function editOffice(id: number) {
    navigate(`/edit_office/${id}`);
  }

  async function loadOffices() {
    const response = await api.get('/office/');
    setOffices(response.data);
  }

  async function deleteOffice(id: number) {
    await api.delete(`/office/${id}`);
    loadOffices();
  }

  return (
    <div>
      <Header title="List of Offices" button="new office" link="/new_office" />
      <Table responsive="md" striped bordered hover size="lg">
        <thead>
          <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {offices.map((office) => (
            <tr key={office.id}>
              <td>{office.id}</td>
              <td>{office.name}</td>
              <td className="text-center">
                <Button
                  variant="info"
                  size="sm"
                  onClick={() => {
                    detailOffice(office.id);
                  }}
                >
                  <FaInfo />
                </Button>{' '}
                <Button
                  variant="success"
                  size="sm"
                  onClick={() => {
                    editOffice(office.id);
                  }}
                >
                  <FaRegEdit />
                </Button>{' '}
                <Button
                  variant="danger"
                  size="sm"
                  onClick={() => {
                    deleteOffice(office.id);
                  }}
                >
                  {' '}
                  <FaTrashAlt />
                </Button>{' '}
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </div>
  );
};

export default OfficeComponent;
